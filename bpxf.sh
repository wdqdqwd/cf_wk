#!/bin/bash
docker exec -it qinglong3 bash
echo "####2.2开始下载js.tar.gz文件到/ql/dist"
wget -t 5 https://gitee.com/suiyuehq/ziyong/raw/master/ql_cdn/v2.10.13/js.tar.gz -P /ql/dist

echo "####2.3解压js.tar.gz文件到/ql/dist/js"
tar -zxvf /ql/dist/js.tar.gz -C /ql/dist

echo "###3、修改index.html文件###"

sed -i 's#https://.*/react/16.*/umd/react.production.*.js#/js/react.production.min.js#g' "/ql/dist/index.html"
sed -i 's#https://.*/react-dom/16.*/umd/react-dom.production.*.js#/js/react-dom.production.min.js#g' "/ql/dist/index.html"
sed -i 's#https://.*/darkreader@4.*/darkreader.*.js#/js/darkreader.min.js#g' "/ql/dist/index.html"
sed -i 's#https://.*/codemirror@5.*/lib/codemirror.*.js#/js/codemirror.min.js#g' "/ql/dist/index.html"
sed -i 's#https://.*/codemirror@5.*/mode/shell/shell.js#/js/shell.js#g' "/ql/dist/index.html"
sed -i 's#https://.*/codemirror@5.*/mode/python/python.js#/js/python.js#g' "/ql/dist/index.html"
sed -i 's#https://.*/codemirror@5.*/mode/javascript/javascript.js#/js/javascript.js#g' "/ql/dist/index.html"
sed -i 's#https://.*/sockjs-client@1.*/dist/sockjs.*.js#/js/sockjs.min.js#g' "/ql/dist/index.html"

echo "###4、删除js.tar.gz压缩包###"
rm -vf /ql/dist/js.tar.gz*

echo -e "\n###恭喜你，白屏修复操作完成，请执行步骤3检验index.html文件内容里的umi.xxx.js和umi.xxx.css与实际是否相同。
如若修复不成功，请先执行步骤4找回对应版本的index.html文件再重试###\n"
echo -e "提醒：
PC端打开【配置文件、脚本管理】出现白屏，请执行选项5，可彻底修复所有问题！(旧版钉子户的福音来啦！)\n"
exit
